package com.pawanhegde.mobileiron.service;

import com.pawanhegde.mobileiron.model.TLV;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Arrays;

/**
 * Created by pawan on 14/04/18.
 */
public class TLVProcessorTest {
    private TLVProcessor instance = new TLVProcessor(Arrays.asList(new UpperCaseProcessor(), new ReplaceProcessor()));

    @DataProvider(name = "tlvProvider")
    public Object[][] tlvProvider() {
        return new Object[][]{
                {new TLV("UPPRCS", "abcde"), "UPPRCS-ABCDE"},
                {new TLV("REPLCE", "123"), "REPLCE-THIS STRING"},
                {new TLV("TAG001", "abcdefgh"), "Type not valid"}};
    }

    @Test(dataProvider = "tlvProvider")
    public void testProcess(TLV tlv, String expectedResponse) {
        Assert.assertEquals(instance.process(tlv), expectedResponse);
    }
}