package com.pawanhegde.mobileiron.service;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * Created by pawan on 14/04/18.
 */
public class TLVParserTest {
    private TLVParser instance = new TLVParser();

    @DataProvider(name = "parseDataProvider")
    public Object[][] parseData() {
        return new Object[][]{
                {"UPPRCS-0005-abcde", 1},
                {"REPLCE-0003-123", 1},
                {"UPPRCS-0008-AbcdefghREPLCE-0003-123REPLCE-0001-Z", 3},
                {"TAG001-0012-abcdefgh1234", 1},
                {"UPPRCS-0004-1234", 1}
        };
    }

    @Test(dataProvider = "parseDataProvider")
    public void testParser(String line, int tlvCount) {
        Assert.assertEquals(instance.parse(line).size(), tlvCount);
    }
}