package com.pawanhegde.mobileiron.model;

/**
 * Created by pawan on 14/04/18.
 */
public class TLV {
    public final String operation;
    public final String value;

    public TLV(String operation, String value) {
        this.operation = operation;
        this.value = value;
    }

    @Override
    public String toString() {
        return "TLV{" +
                "operation='" + operation + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
