package com.pawanhegde.mobileiron;

import com.pawanhegde.mobileiron.service.TLVParser;
import com.pawanhegde.mobileiron.service.TLVProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

import java.util.Scanner;

/**
 * Created by pawan on 14/04/18.
 */
@SpringBootApplication
public class TLVFeeder implements ApplicationRunner {

    @Autowired
    private TLVProcessor tlvProcessor;

    @Autowired
    private TLVParser tlvParser;

    public static void main(String[] args) {
        new SpringApplicationBuilder()
                .main(TLVFeeder.class)
                .sources(TLVFeeder.class)
                .bannerMode(Banner.Mode.OFF)
                .logStartupInfo(false)
                .web(false)
                .build()
                .run(args);
    }

    @Override
    public void run(ApplicationArguments applicationArguments) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("----- Input after this line ----");

        while (scanner.hasNextLine()) {
            tlvParser.parse(scanner.nextLine().trim()).stream()
                    .map(tlvProcessor::process)
                    .forEach(System.out::println);
        }

        System.out.println("----- Application Terminated ----");
    }
}
