package com.pawanhegde.mobileiron.service;

import com.pawanhegde.mobileiron.model.TLV;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by pawan on 14/04/18.
 */

@Service
public class TLVProcessor {

    private List<ValueProcessor> processors;

    @Autowired
    TLVProcessor(List<ValueProcessor> processors) {
        this.processors = processors;
    }

    public String process(final TLV tlv) {
        Optional<? extends ValueProcessor> processorOptional = processors.parallelStream()
                .filter(p -> p.supports(tlv.operation))
                .findAny();

        if (!processorOptional.isPresent()) {
            return "Type not valid";
        }

        ValueProcessor processor = processorOptional.get();

        return processor.process(tlv.value);
    }
}
