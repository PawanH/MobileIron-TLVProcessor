package com.pawanhegde.mobileiron.service;

import com.pawanhegde.mobileiron.model.TLV;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pawan on 14/04/18.
 */

@Service
public class TLVParser {
    public List<TLV> parse(String line) {
        List<TLV> tlvs = new ArrayList<>();

        try {
            int offset = 0;

            while (offset < line.length()) {
                String operator = line.substring(offset, offset + 6);
                int length = Integer.parseInt(line.substring(offset + 7, offset + 11));
                String value = line.substring(offset + 12, offset + 12 + length);

                offset += 12 + length;

                TLV tlv = new TLV(operator, value);
                tlvs.add(tlv);
            }
        } catch (Exception e) {
            System.err.println("I was told to assume that I'd not have to test for malformed TLVs. Did you lie to me, MobileIron? :(");
        }

        return tlvs;
    }
}
