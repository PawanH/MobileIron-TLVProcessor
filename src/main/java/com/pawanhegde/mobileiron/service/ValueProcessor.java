package com.pawanhegde.mobileiron.service;

/**
 * Created by pawan on 14/04/18.
 */
public interface ValueProcessor {
    boolean supports(String operator);

    String process(String value);
}
