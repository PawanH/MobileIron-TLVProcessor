package com.pawanhegde.mobileiron.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * Created by pawan on 14/04/18.
 */
@Service
public class ReplaceProcessor implements ValueProcessor {
    private String supportedOperator = "REPLCE";

    @Override
    public boolean supports(String operator) {
        return StringUtils.equalsIgnoreCase(operator, supportedOperator);
    }

    @Override
    public String process(String value) {
        return "REPLCE-THIS STRING";
    }
}
